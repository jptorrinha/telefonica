$(document).ready(function () {
	
	//Script para gerar um array no console e na página do value do SKU dos produtos no load de página!
	var $pdt = $('#produtos').find('a');
	$.each($pdt, function (index, value) { 
	    console.log($(value).data('sku'));
	    $('#skus-list').append('<pre>SKU '+ index +': ' + $(value).data('sku') + '</pre>');
	});

	
	$pdt.bind('click', function (e) {

		var obj = { 
			nome: $(this).find('.name').text(), 
			preco: $(this).find('.price').text(), 
			sku: $(this).data('sku')
		};

		console.log(obj);

		e.preventDefault();
	});
});