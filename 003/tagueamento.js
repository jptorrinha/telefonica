$(document).ready(function () {

	var $btns = $('#botoes').find('button');

	//Script para eibir no front-end o nome de cada botão quando clicado!
	$btns.on('click', function(index, event){
	    var $clickbn = $(this).text().split('-')[2];
	    $(this).attr("disabled", true);
	    $('#retorno ul').append('<li>' + $clickbn + '</li>');
	    console.log($(this).text().split('-')[2]);
	});

	//Criação das cariáveis do projeto
	//var arrClick vazia para ser alimentada quando é feito o evento de click
	//var buttons recebe os valores do atributo button
	//var str string vazia para ser alientada quando todos os clicques forem feitos
	//var div para criar o elemento para mostrar no front end a ordem dos cliques
	var buttons = [].slice.call(document.getElementById('botoes').children);
	var arrClick = [];
	var str = '';
	var elementBtn = document.getElementById('botoes');
	var div = document.createElement('p');

	//forEach dos botões, adicionando o evento de click na var element
	buttons.forEach(function (element, i){
	  	element.addEventListener("click", function(){

		  	//var de retorno para criar o log do retorno solicitado na questão
		  	var retorno = buttons[i].innerHTML.split('-');
		  	console.log(retorno[0]);

		  	//verificação se a var arrClick está vazia, se ele estiver fazia faço um push para preencher ela com o valor do index do botão
		  	if(arrClick.indexOf(buttons[i]) == -1){
				arrClick.push(buttons[i]);

				//alimento a var str com a ordem dos botões clicados
				str+=i+' ';


				//verifico se arrClick é >= a buttons e crio o elemento div para imprimir no front-en a orden dos cliques
				if(arrClick.length >= buttons.length){
					div.append('Orden do click: '+ str);
					elementBtn.insertAdjacentElement('afterend', div);
				}
			}
	  	});
	});
});