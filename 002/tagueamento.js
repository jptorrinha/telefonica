$(document).ready(function () {
	
	//Script para gerar um array no console e na página do value do botão no load de página!
	var $btn = $('#valores').find('span');
	$.each($btn, function (index, value) { 
	    //console.log(index + ':' + $(value).text());
	    $('#array').append('<pre>Box ' + index + ': ' + $(value).text() + '</pre>');
	});

	//Script para eibir no front-end o nome de cada botão quando clicado!
	$btn = $('#botoes').find('a');
	$btn.on('click', function (e) {

		var indexBtn =  $(this).closest('#botoes').find('a').index( this );
		var indexVal = $( $("#valores").find('span').get( indexBtn ));

		$(this).append('<i>' + indexVal.text() + '</i>');

		console.log('BTN: ' + indexBtn);
		console.log('Val: ' + indexVal.text());

		e.preventDefault();
	});
});