$(document).ready(function () {
	
	//Script para gerar um array no console e na página do value do botão no load de página!
	var $btn = $('#botoes').find('button');
	$.each($btn, function (index, value) { 
	    console.log(index + ':' + $(value).text());
	    $('#array').append('<pre>Botão ' + index + ': ' + $(value).text() + '</pre>');
	});

	//Script para eibir no front-end o nome de cada botão quando clicado!
	$('#botoes').find('button').on('click', function(){
	    var $clickbn = $(this).text();
	    $('#botoes').find('button').removeClass('active');
	    $(this).addClass('active');
	    $('#retorno ul li').hide();
	    $('#retorno ul').append('<li>' + $clickbn + '</li>');
	    console.log($(this).text());
	});
});